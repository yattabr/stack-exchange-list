# Stack Exchange List
This Project is a sample of an user list from Stack Exchange.

### Requires
Display an input field and Button to search for users by name.  
Display up to 20 users alphabetically and show their reputation and username.  
When tapped, open a new `Activity` to display more information about the
user.
Display a `EditText` to serch by Username. 
When the username is typed, the list is filtered automatically.
 
### Libraries Used
- Dagger 2
- Retrofit 2
- RxJava 2
- Glide

### Created by
Eduardo Ewerton
