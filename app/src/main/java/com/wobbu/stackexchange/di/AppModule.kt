package com.wobbu.stackexchange.di

import android.content.Context
import com.wobbu.stackexchange.main.UserAdapter
import dagger.Module
import javax.inject.Singleton
import dagger.Provides

@Module
class AppModule(private val context: Context) {

    @Provides
    @Singleton
    internal fun provideContext(): Context {
        return context
    }

    @Provides
    internal fun usersAdapter(): UserAdapter {
        return UserAdapter(context)
    }
}