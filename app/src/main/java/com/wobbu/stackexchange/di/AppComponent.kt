package com.wobbu.stackexchange.di

import com.wobbu.stackexchange.main.MainActivity
import dagger.Component
import javax.inject.Singleton


@Component(modules = [AppModule::class, UtilsModule::class])
@Singleton
interface AppComponent {
    fun doInjection(mainActivity: MainActivity)
}
