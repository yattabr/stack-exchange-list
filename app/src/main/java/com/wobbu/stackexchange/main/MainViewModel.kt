package com.wobbu.stackexchange.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.wobbu.stackexchange.data.ApiResponse
import com.wobbu.stackexchange.data.Repository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainViewModel(var repository: Repository) : ViewModel() {

    val fetchUsersObserver = MutableLiveData<Any>()
    private val disposables = CompositeDisposable()

    fun fetchUsers() {
        disposables.add(repository.fetchUsers()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { fetchUsersObserver.setValue(ApiResponse.loading()) }
            .subscribe(
                { result -> fetchUsersObserver.setValue(ApiResponse.success(result)) },
                { throwable -> fetchUsersObserver.setValue(ApiResponse.error(throwable)) }
            ))
    }

    fun searchUsers(username: String = "") {
        disposables.add(repository.searchUsers(username)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { fetchUsersObserver.setValue(ApiResponse.loading()) }
            .subscribe(
                { result -> fetchUsersObserver.setValue(ApiResponse.success(result)) },
                { throwable -> fetchUsersObserver.setValue(ApiResponse.error(throwable)) }
            ))
    }


}