package com.wobbu.stackexchange.main

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.wobbu.stackexchange.R
import com.wobbu.stackexchange.base.BaseApplication
import com.wobbu.stackexchange.base.ViewModelFactory
import com.wobbu.stackexchange.data.ApiResponse
import com.wobbu.stackexchange.data.Status.*
import com.wobbu.stackexchange.model.Users
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    @Inject
    lateinit var userAdapter: UserAdapter
    lateinit var mainViewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        (application as BaseApplication).appComponent.doInjection(this)
        mainViewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)

        edit_search.addTextChangedListener(filterWatcher)
        bt_search.setOnClickListener(searchClick)

        initObservers()
        mainViewModel.fetchUsers()
    }

    private fun initObservers() {
        mainViewModel.fetchUsersObserver.observe(this, Observer {
            when (it) {
                is ApiResponse -> {
                    consumeAPI(it)
                }
            }
        })
    }

    private fun consumeAPI(apiResponse: ApiResponse) {
        when (apiResponse.status) {
            SUCCESS -> {
                val users = Gson().fromJson(apiResponse.data, Users::class.java)
                mountRecyclerView(users)
            }

            LOADING -> {
                loading.visibility = View.VISIBLE
            }

            ERROR -> {
                loading.visibility = View.GONE
                Toast.makeText(this, "Some error happened.", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private val filterWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            userAdapter.filter.filter(s)
        }

    }

    private fun mountRecyclerView(users: Users) {
        userAdapter.updateUsers(users)
        recycler_users.adapter = userAdapter
        loading.visibility = View.GONE
    }

    private val searchClick = View.OnClickListener {
        val username = edit_search.text.toString()
        mainViewModel.searchUsers(username)
    }
}
