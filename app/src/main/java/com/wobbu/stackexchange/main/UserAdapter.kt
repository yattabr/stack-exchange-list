package com.wobbu.stackexchange.main

import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.wobbu.stackexchange.R
import com.wobbu.stackexchange.model.User
import com.wobbu.stackexchange.model.Users
import com.wobbu.stackexchange.userDetail.UserDetailActivity
import kotlinx.android.synthetic.main.item_users.view.*
import java.util.*
import kotlin.collections.ArrayList

class UserAdapter(var providerContext: Context) :
    RecyclerView.Adapter<UserAdapter.CustomViewHolder>(), Filterable {

    private var users: Users? = null
    private var filteredList: ArrayList<User> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_users, parent, false)
        return CustomViewHolder(providerContext, view)
    }

    fun updateUsers(users: Users) {
        this.users = users
        filteredList = users.items
    }

    override fun getItemCount(): Int {
        return filteredList.size
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        filteredList.sortBy { it.username }
        holder.bind(filteredList[position])
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charString = constraint.toString()
                filteredList = if (charString.isEmpty()) {
                    users!!.items
                } else {
                    val list = users!!.items.filter { item ->
                        item.username.toLowerCase(Locale.getDefault())
                            .contains(charString.toLowerCase(Locale.getDefault()))
                    }
                    list as ArrayList<User>
                }
                val filterResults = FilterResults()
                filterResults.values = filteredList
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                filteredList = results!!.values as ArrayList<User>
                notifyDataSetChanged()
            }

        }
    }

    class CustomViewHolder(var context: Context, itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        fun bind(user: User) {
            itemView.txt_reputation.text = "${user.reputation} Reputation"
            itemView.txt_name.text = "${user.username}"
            Glide.with(context).load(user.profileImage).into(itemView.img_profile)

            itemView.setOnClickListener {
                val intent = Intent(context, UserDetailActivity::class.java)
                intent.addFlags(FLAG_ACTIVITY_NEW_TASK)
                intent.putExtra("user", user)
                intent.putExtra("badges", user.badgeCounts)
                context.startActivity(intent)
            }
        }
    }
}