package com.wobbu.stackexchange.data

import com.google.gson.JsonElement
import io.reactivex.Observable


class Repository(private val apiCallInterface: ApiCallInterface) {
    fun fetchUsers(): Observable<JsonElement> {
        return apiCallInterface.fetchUsers()
    }

    fun searchUsers(username: String = ""): Observable<JsonElement> {
        return apiCallInterface.searchUsers(username)
    }

}