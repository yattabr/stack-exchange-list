package com.wobbu.stackexchange.data

class Urls {
    companion object {

        const val BASE_URL = "https://api.stackexchange.com/2.2/"
        const val FETCH_USERS = "users?pagesize=20&order=desc&sort=reputation&site=stackoverflow"
    }
}