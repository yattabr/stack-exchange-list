package com.wobbu.stackexchange.data

import com.google.gson.JsonElement
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiCallInterface {

    @GET(Urls.FETCH_USERS)
    fun fetchUsers(): Observable<JsonElement>

    @GET(Urls.FETCH_USERS)
    fun searchUsers(@Query("inname") username: String = ""): Observable<JsonElement>
}