package com.wobbu.stackexchange.data

enum class Status {
    LOADING,
    SUCCESS,
    ERROR
}