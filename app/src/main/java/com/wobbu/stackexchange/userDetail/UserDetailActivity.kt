package com.wobbu.stackexchange.userDetail

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.wobbu.stackexchange.R
import com.wobbu.stackexchange.model.Badges
import com.wobbu.stackexchange.model.User
import kotlinx.android.synthetic.main.activity_user_detail.*

class UserDetailActivity : AppCompatActivity() {

    lateinit var user: User
    lateinit var badges: Badges

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_detail)

        user = intent!!.getParcelableExtra("user")
        badges = intent!!.getParcelableExtra("badges")

        initUI()
    }

    private fun initUI() {
        txt_username.text = user.username
        txt_reputation.text = "Reputation: ${user.reputation}"
        txt_location.text = user.location
        txt_creation_date.text = user.creationDate
        txt_gold_badge.text = badges.gold
        txt_silver_badge.text = badges.silver
        txt_bronze_badge.text = badges.bronze

        Glide.with(this).load(user.profileImage).into(img_profile)
    }
}