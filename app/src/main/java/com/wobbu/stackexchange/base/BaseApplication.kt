package com.wobbu.stackexchange.base

import android.app.Application
import android.content.Context
import com.wobbu.stackexchange.di.AppComponent
import com.wobbu.stackexchange.di.AppModule
import com.wobbu.stackexchange.di.DaggerAppComponent
import com.wobbu.stackexchange.di.UtilsModule

class BaseApplication : Application() {
    lateinit var appComponent: AppComponent
    lateinit var context: Context

    override fun onCreate() {
        super.onCreate()
        context = this
        appComponent =
            DaggerAppComponent.builder().appModule(AppModule(this)).utilsModule(UtilsModule())
                .build()
    }


    protected override fun attachBaseContext(context: Context) {
        super.attachBaseContext(context)
    }
}