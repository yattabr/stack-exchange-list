package com.wobbu.stackexchange.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.Gson
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.lang.reflect.Type

class Users {
    var items: ArrayList<User> = arrayListOf()
}

class Badges() : Parcelable {
    var bronze: String = ""
    var silver: String = ""
    var gold: String = ""

    constructor(parcel: Parcel) : this() {
        bronze = parcel.readString()
        silver = parcel.readString()
        gold = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(bronze)
        parcel.writeString(silver)
        parcel.writeString(gold)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Badges> {
        override fun createFromParcel(parcel: Parcel): Badges {
            return Badges(parcel)
        }

        override fun newArray(size: Int): Array<Badges?> {
            return arrayOfNulls(size)
        }
    }
}

open class User() : Parcelable {
    @SerializedName("display_name")
    var username: String = ""
    var reputation: String = ""
    var location: String = ""
    @SerializedName("creation_date")
    var creationDate: String = ""
    @SerializedName("profile_image")
    var profileImage: String = ""
    @SerializedName("badge_counts")
    var badgeCounts: Badges = Badges()

    constructor(parcel: Parcel) : this() {
        username = parcel.readString()
        reputation = parcel.readString()
        location = parcel.readString()
        creationDate = parcel.readString()
        profileImage = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(username)
        parcel.writeString(reputation)
        parcel.writeString(location)
        parcel.writeString(creationDate)
        parcel.writeString(profileImage)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<User> {
        override fun createFromParcel(parcel: Parcel): User {
            return User(parcel)
        }

        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }
    }
}

open class DeserializerUsers : JsonDeserializer<Users> {
    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): Users {
        val users = Users()
        Observable.just(json.toString())
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread()).map {
                Gson().fromJson(it, Users::class.java)
            }
            .flatMap { jsonObject ->
                Observable.fromArray(jsonObject)
            }
            .blockingSubscribe { listUsers ->
                users.items.addAll(listUsers.items)
            }

        return users
    }

}